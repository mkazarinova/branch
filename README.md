Technical Requirements: 
Java 17

### Build and Run instructions:
1. Clone repository: 
```
   git clone git@gitlab.com:mkazarinova/branch.git
```
2. Go to project's root directory:
```
cd branch
```
3. Build project and assemble executable jar:
```
gradlew clean build or ./gradlew clean build
```
4. Start application (application will run on port 8080):
```
java -jar build/libs/branch-1.0-SNAPSHOT.jar
```
5. Available endpoint URI: 
```
/users/{username}
```

### Expectations:
1. Given existing username is provided the endpoint will respond with 200 OK and body with user data in JSON format. User data will be stored in cache.
2. Given requested user data doesn't exist in cache yet and API calls to Github API resulted in any errors the endpoint will respond with corresponding error response code and error details in the response body.
3. Given requested user data exist in cache and API calls to Github API resulted in any errors the endpoint will respond with 200 OK and body with user data in JSON format.

