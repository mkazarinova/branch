package org.branch.codingexcercise.exceptions;

public class GithubException extends Throwable {
    public final int statusCode;
    public GithubException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }
}
