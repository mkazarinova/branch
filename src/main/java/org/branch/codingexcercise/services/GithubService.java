package org.branch.codingexcercise.services;

import lombok.extern.slf4j.Slf4j;
import org.branch.codingexcercise.models.UserData;
import org.branch.codingexcercise.models.GithubUserInfo;
import org.branch.codingexcercise.models.GithubUserRepo;
import org.branch.codingexcercise.webclients.GithubClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import java.util.List;

@Slf4j
@Service
public class GithubService {
    private final String userDataCacheName;
    private final CacheManager cacheManager;
    private final GithubClient githubClient;

    public GithubService(CacheManager cacheManager, GithubClient githubClient, @Value("${spring.cache.userdata}") String userDataCacheName) {
        this.cacheManager = cacheManager;
        this.githubClient = githubClient;
        this.userDataCacheName = userDataCacheName;
    }

    public Mono<UserData> getUserData(String username) {
        UserData userDataFromCache = getUserDataFromCache(username);
        if(userDataFromCache != null)
            return Mono.just(userDataFromCache);
        Mono<GithubUserInfo> userInfoMono = githubClient.getUserInfo(username)
                .doOnError(error -> log.error(error.getMessage()));
        Mono<List<GithubUserRepo>> userReposMono = githubClient.getUserRepos(username)
                .doOnError(error -> log.error(error.getMessage()));
        return Mono.zip(userInfoMono, userReposMono)
                .map(data -> {
                    UserData userdata = new UserData(data.getT1(), data.getT2());
                    cacheUserData(userdata);
                    return userdata;
                });

    }

    public UserData getUserDataFromCache(String username) {
        Cache userDataCache = cacheManager.getCache(userDataCacheName);
        if(userDataCache == null)
            return null;
        Cache.ValueWrapper valueWrapper = userDataCache.get(username);
        if(valueWrapper != null) {
            return (UserData) valueWrapper.get();
        } else {
            return null;
        }
    }

    private void cacheUserData(UserData userData) {
        Cache userDataCache = cacheManager.getCache(userDataCacheName);
        if(userDataCache == null || userData == null || userData.getUsername() == null)
            return;
        userDataCache.put(userData.getUsername(), userData);
    }
}
