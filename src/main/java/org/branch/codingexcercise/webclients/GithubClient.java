package org.branch.codingexcercise.webclients;

import org.branch.codingexcercise.exceptions.GithubException;
import org.branch.codingexcercise.models.GithubError;
import org.branch.codingexcercise.models.GithubUserInfo;
import org.branch.codingexcercise.models.GithubUserRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class GithubClient {
    private final WebClient webclient;

    public GithubClient(@Value("${github.api.base.url}") String githubAPIBaseUrl) {
        this.webclient = WebClient.builder()
                .baseUrl(githubAPIBaseUrl)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public Mono<GithubUserInfo> getUserInfo(String username) {
        return webclient.get()
                .uri("/users/" + username)
                .retrieve()
                .onStatus(
                        HttpStatusCode::is5xxServerError,
                        response -> response.bodyToMono(GithubError.class)
                                .map(errorResponse -> new GithubException("Error retrieving user info: " + errorResponse.getMessage(), response.statusCode().value()))
                )
                .onStatus(
                        HttpStatusCode::is4xxClientError,
                        response -> response.bodyToMono(GithubError.class)
                                .map(errorResponse -> new GithubException("Error retrieving user info: " + errorResponse.getMessage(), response.statusCode().value()))
                )
                .bodyToMono(GithubUserInfo.class);
    }

    public Mono<List<GithubUserRepo>> getUserRepos(String username) {
        return webclient.get()
                .uri("/users/" + username + "/repos")
                .retrieve()
                .onStatus(
                        HttpStatusCode::is5xxServerError,
                        response -> response.bodyToMono(GithubError.class)
                                .map(errorResponse -> new GithubException("Error retrieving user repos: " + errorResponse.getMessage(), response.statusCode().value()))
                )
                .onStatus(
                        HttpStatusCode::is4xxClientError,
                        response -> response.bodyToMono(GithubError.class)
                                .map(errorResponse -> new GithubException("Error retrieving user repos: " + errorResponse.getMessage(), response.statusCode().value()))
                )
                .bodyToFlux(GithubUserRepo.class).collectList();
    }
}
