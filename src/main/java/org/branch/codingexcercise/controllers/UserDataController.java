package org.branch.codingexcercise.controllers;

import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.branch.codingexcercise.models.UserData;
import org.branch.codingexcercise.services.GithubService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users")
@Slf4j
@Validated
public class UserDataController {
    private final GithubService githubService;

    public UserDataController(GithubService githubService) {
        this.githubService = githubService;
    }

    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mono<UserData>> getUserData(@PathVariable @NotBlank String username) {
        Mono<UserData> userInfoMono = githubService.getUserData(username);

        return ResponseEntity.ok(userInfoMono);
    }
}
