package org.branch.codingexcercise.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateFormatter {
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone( ZoneId.of("UTC") );

    public static String reformatDate(String date){
        if(date == null){
            return null;
        }
        try {
            Instant instant = Instant.parse(date);
            return formatter.format(instant);
        } catch (Exception e){
            return null;
        }
    }
}
