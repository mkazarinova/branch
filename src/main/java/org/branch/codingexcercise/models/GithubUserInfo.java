package org.branch.codingexcercise.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude
public class GithubUserInfo {
    @JsonProperty("login")
    private String username;

    @JsonProperty("name")
    private String displayName;

    private String url;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    private String location;

    private String email;

    @JsonProperty("created_at")
    private String createdAt;
}
