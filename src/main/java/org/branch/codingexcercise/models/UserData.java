package org.branch.codingexcercise.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.branch.codingexcercise.utils.DateFormatter;
import java.util.Collections;
import java.util.List;

@Getter
@NoArgsConstructor
public class UserData {
    @JsonProperty("user_name")
    private String username = null;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("avatar")
    private String avatarUrl;
    @JsonProperty("geo_location")
    private String location;
    @JsonProperty("email")
    private String email;
    @JsonProperty("url")
    private String url;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("repos")
    private List<GithubUserRepo> repos = Collections.emptyList();

    public UserData(GithubUserInfo userInfo, List<GithubUserRepo> githubUserRepos) {
        this.username = userInfo.getUsername();
        this.displayName = userInfo.getDisplayName();
        this.avatarUrl = userInfo.getAvatarUrl();
        this.location = userInfo.getLocation();
        this.email = userInfo.getEmail();
        this.url = userInfo.getUrl();
        this.createdAt = DateFormatter.reformatDate(userInfo.getCreatedAt());
        this.repos = githubUserRepos;
    }
}
