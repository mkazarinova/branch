package org.branch.codingexcercise.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.branch.codingexcercise.exceptions.GithubException;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(GithubException.class)
    public ResponseEntity<ApiError> handleException(GithubException ex) {
        return ResponseEntity.status(HttpStatusCode.valueOf(ex.statusCode)).body(new ApiError(ex.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> serverExceptionHandler(Exception ex) {
        return ResponseEntity.status(HttpStatusCode.valueOf(500)).body(new ApiError(ex.getMessage()));
    }
}

@Getter
class ApiError {
    private final String error;
    public ApiError(String error){
        this.error = error;
    }
}

