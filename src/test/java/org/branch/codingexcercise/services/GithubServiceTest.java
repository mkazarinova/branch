package org.branch.codingexcercise.services;

import org.branch.codingexcercise.exceptions.GithubException;
import org.branch.codingexcercise.models.UserData;
import org.branch.codingexcercise.models.GithubUserInfo;
import org.branch.codingexcercise.models.GithubUserRepo;
import org.branch.codingexcercise.webclients.GithubClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import reactor.core.publisher.Mono;

import java.util.List;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class GithubServiceTest {
    @Mock
    private GithubClient githubClient;

    @Autowired
    private CacheManager cacheManager;

    @Value("${spring.cache.userdata}")
    private String userDataCacheName;

    private final String username = "username";
    private final String createdAtFromGithub = "2024-01-26T10:36:00Z";
    private final String expectedCreatedAtOutput = "2024-01-26 10:36:00";
    private final GithubUserInfo userInfoGithubResponse = new GithubUserInfo(
            username,
            "User Name",
            "https://avatar.url",
            "https://user.profile.url",
            "Chicago",
            "user@domain.com",
            createdAtFromGithub
    );

    private final GithubUserRepo githubUserRepo1 = new GithubUserRepo("repo1", "https://user/repo1");
    private final GithubUserRepo githubUserRepo2 = new GithubUserRepo("repo2", "https://user/repo2");
    private final UserData expectedUserData = new UserData(userInfoGithubResponse, List.of(githubUserRepo1, githubUserRepo2));

    @Test
    void testHappyPath() {
        GithubService githubService = new GithubService(cacheManager, githubClient, userDataCacheName);
        Mockito.when(githubClient.getUserInfo(Mockito.anyString())).thenReturn(Mono.just(userInfoGithubResponse));
        Mockito.when(githubClient.getUserRepos(Mockito.anyString())).thenReturn(Mono.just(List.of(githubUserRepo1, githubUserRepo2)));

        UserData userData = githubService.getUserData("username").block();
        Assertions.assertNotNull(userData);
        Assertions.assertEquals(userInfoGithubResponse.getUsername(), userData.getUsername());
        Assertions.assertEquals(userInfoGithubResponse.getDisplayName(), userData.getDisplayName());
        Assertions.assertEquals(userInfoGithubResponse.getUrl(), userData.getUrl());
        Assertions.assertEquals(userInfoGithubResponse.getAvatarUrl(), userData.getAvatarUrl());
        Assertions.assertEquals(userInfoGithubResponse.getLocation(), userData.getLocation());
        Assertions.assertEquals(userInfoGithubResponse.getEmail(), userData.getEmail());
        Assertions.assertEquals(expectedCreatedAtOutput, userData.getCreatedAt());
        Assertions.assertEquals(2, userData.getRepos().size());
    }

    @Test
    void testPropagateException() {
        cacheManager.getCache(userDataCacheName).evictIfPresent(username);
        GithubService githubService = new GithubService(cacheManager, githubClient, userDataCacheName);
        Mockito.when(githubClient.getUserInfo(Mockito.anyString())).thenReturn(Mono.error(new GithubException("Github not happy exception", 500)));
        Mockito.when(githubClient.getUserRepos(Mockito.anyString())).thenReturn(Mono.just(List.of(githubUserRepo1, githubUserRepo2)));
        Exception thrownException = Assertions.assertThrows(Exception.class,
                () -> githubService.getUserData(username).block()
        );
        Assertions.assertInstanceOf(GithubException.class, thrownException.getCause());
    }

    @Test
    void testGetUserDataFromCacheAndNotCallGithubApi() {
        cacheManager.getCache(userDataCacheName).evictIfPresent(username);
        GithubService githubService = new GithubService(cacheManager, githubClient, userDataCacheName);
        Mockito.when(githubClient.getUserInfo(Mockito.anyString())).thenReturn(Mono.just(userInfoGithubResponse));
        Mockito.when(githubClient.getUserRepos(Mockito.anyString())).thenReturn(Mono.just(List.of(githubUserRepo1, githubUserRepo2)));

        // call getUserData and verify web client made calls to fetch the data
        UserData userData = githubService.getUserData(username).block();
        Mockito.verify(githubClient, Mockito.times(1)).getUserInfo(Mockito.anyString());
        Mockito.verify(githubClient, Mockito.times(1)).getUserRepos(Mockito.anyString());
        Assertions.assertNotNull(userData);
        Assertions.assertEquals(userInfoGithubResponse.getDisplayName(), userData.getDisplayName());
        Assertions.assertEquals(2, userData.getRepos().size());

        // call getUserData again for the same username and verify no web client call were made (retrieved from cache)
        Mockito.clearInvocations(githubClient);
        userData = githubService.getUserData(username).block();
        Mockito.verify(githubClient, Mockito.times(0)).getUserInfo(Mockito.anyString());
        Mockito.verify(githubClient, Mockito.times(0)).getUserRepos(Mockito.anyString());
        Assertions.assertNotNull(userData);
        Assertions.assertEquals(userInfoGithubResponse.getDisplayName(), userData.getDisplayName());
        Assertions.assertEquals(2, userData.getRepos().size());
    }

    @Test
    void testGetUserDataFromCache() {
        cacheManager.getCache(userDataCacheName).put(username, expectedUserData);
        GithubService githubService = new GithubService(cacheManager, githubClient, userDataCacheName);

        UserData userData = githubService.getUserDataFromCache(username);
        Assertions.assertNotNull(userData);
        Assertions.assertEquals(userInfoGithubResponse.getDisplayName(), userData.getDisplayName());
        Assertions.assertEquals(2, userData.getRepos().size());

        UserData nonExistingUserData = githubService.getUserDataFromCache("userNotInCache");
        Assertions.assertNull(nonExistingUserData);

    }
}
