package org.branch.codingexcercise.controllers;

import org.branch.codingexcercise.config.ApiExceptionHandler;
import org.branch.codingexcercise.exceptions.GithubException;
import org.branch.codingexcercise.models.GithubUserInfo;
import org.branch.codingexcercise.models.UserData;
import org.branch.codingexcercise.services.GithubService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserDataControllerTest {
    @Mock
    private GithubService githubService;

    WebTestClient webTestClient;
    private final String username = "someuser";

    @BeforeEach
    void setup() {
        webTestClient =
                WebTestClient.bindToController(new UserDataController(githubService)).controllerAdvice(ApiExceptionHandler.class
                ).build();
        Mockito.clearInvocations(githubService);
    }

    @Test
    void testHappyPath(){
        String errorMessage = "some random upstream error";
        Mockito.when(githubService.getUserData(Mockito.anyString())).thenReturn(Mono.just(new UserData(new GithubUserInfo(username, null, null, null, null, null, null), Collections.emptyList())));
        webTestClient.get()
                .uri("/users/" + username)
                .exchange()
                .expectStatus()
                .isEqualTo(200)
                .expectBody()
                .jsonPath("$.user_name")
                .isEqualTo(username);
    }

    @Test
    void testErrorResponseForGithubErrors(){
        String errorMessage = "some random upstream error";
        Mockito.when(githubService.getUserData(Mockito.anyString())).thenReturn(Mono.error(new GithubException(errorMessage, 404)));
        webTestClient.get()
                .uri("/users/" + username)
                .exchange()
                .expectStatus()
                .isEqualTo(404)
                .expectBody()
                .returnResult()
                .getResponseBody()
                .toString()
                .contains(errorMessage);
    }

    @Test
    void testErrorResponseForUnexpectedErrors(){
        String errorMessage = "oops";
        Mockito.when(githubService.getUserData(Mockito.anyString())).thenReturn(Mono.error(new RuntimeException(errorMessage)));
        webTestClient.get()
                .uri("/users/" + username)
                .exchange()
                .expectStatus()
                .isEqualTo(500)
                .expectBody()
                .returnResult()
                .getResponseBody()
                .toString()
                .contains(errorMessage);
    }
}
