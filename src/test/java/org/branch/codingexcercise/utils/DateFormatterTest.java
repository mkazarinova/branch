package org.branch.codingexcercise.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateFormatterTest {
    @Test
    void testDateFormatter() {
        Assertions.assertNull(DateFormatter.reformatDate("foo"));
        Assertions.assertNull(DateFormatter.reformatDate(""));
        Assertions.assertNull(DateFormatter.reformatDate(null));
        Assertions.assertEquals("2024-02-29 16:41:33", DateFormatter.reformatDate("2024-02-29T16:41:33Z"));
    }
}
